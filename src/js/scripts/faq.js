$(document).ready(function () {
    initFaq();
});

const collapsedClass = 'box-collapsed';
let $questions = null;
let $active = null;
let isCollapsing = false;

function initFaq() {
    $questions = $('.js-question');

    $.each($questions, (index, element) => {
        $(element).attr('status', 'hidden');
    });

    $.each($questions, (index, question) => {
        $(question).click(() => {
            if (!isCollapsing) {
                this.isCollapsing = true;
                const status = $(question).attr('status');

                switch (status) {
                    case 'collapsing':
                        return;
                    case 'hidden':
                        show(question);
                        return;
                    case 'shown':
                        hide(question);
                        return;
                }
            }
        });
    });
}

function hide(question) {
    const answer = $($(question).attr('href'));

    answer.collapse('hide');
    $(question).addClass(collapsedClass);
    setStatus(question, 'collapsing');

    answer.on('hidden.bs.collapse', () => {
        setStatus(question, 'hidden');
        this.isCollapsing = false;
    });
}

function show(question) {
    const answer = $($(question).attr('href'));

    answer.collapse('show');
    setStatus(question, 'collapsing');
    $(question).removeClass(collapsedClass);

    answer.on('shown.bs.collapse', () => {
        setStatus(question, 'shown');
        this.isCollapsing = false;
    });
}

function setStatus(element, status) {
    $(element).attr('status', status);
}