$(document).ready(function () {
    $('[ga-event]').click(function () {
        gtag('event', $(this).data('action'), {
            'event_category': $(this).data('category')
        });
    });
});