$(window).on('scroll', function () {

    var position = $(this).scrollTop();
    var prizesHeight = $('#prizes').height();
    // console.log(position)

    $('.js-box-animate').each((index, element) => {
        if (position >= ($(element).offset().top - prizesHeight)) {
            $(element).removeClass('box-collapsed');
        }
    })

});