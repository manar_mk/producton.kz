$(document).ready(function () {
    $('.js-countdown').countdown('2019/04/14', function (event) {
        $(this).html(constructDate(event.offset));
    });
});


function constructDate(offset) {
    return `${getDaysString(offset.totalDays)}${getHoursEnding(offset.hours)}, ${getMinutesEnding(offset.minutes)}`;
}

function getDaysString(number) {
    if (number === 0) {
        return '';
    } else if (number < 20 && number >= 5 || number > 20 && number % 10 >= 5 || number % 10 === 0) {
        return `${number} дней, `;
    } else if (number % 10 > 1 && number % 10 < 5) {
        return `${number} дня, `;
    } else if (number % 10 === 1) {
        return `${number} день, `;
    }
}


function getHoursEnding(number) {
    if (number < 20 && number >= 5 || number > 20 && number % 10 >= 5 || number % 10 === 0) {
        return `${number} часов`;
    } else if (number % 10 > 1 && number % 10 < 5) {
        return `${number} часа`;
    } else if (number % 10 === 1) {
        return `${number} час`;
    }
}

function getMinutesEnding(number) {
    if (number < 20 && number >= 5 || number > 20 && number % 10 >= 5 || number % 10 === 0) {
        return `${number} минут`;
    } else if (number % 10 > 1 && number % 10 < 5) {
        return `${number} минуты`;
    } else if (number % 10 === 1) {
        return `${number} минута`;
    }
}